### init
docker run \
    --rm \
    -e TMHOME=/config/tendermint \
    -v $PWD/config/tendermint/node1:/config/tendermint \
    ofp/dpki-blockchain:latest init

### show_node_id
docker run \
    --rm \
    -e TMHOME=/config/tendermint \
    -v $PWD/config/tendermint/node1:/config/tendermint \
    ofp/dpki-blockchain:latest show-node-id

### node
docker run \
    --rm \
    -e ABCI_DB_DIR_PATH=/DB \
    -e TMHOME=/config/tendermint \
    -v $PWD/config/tendermint/node1:/config/tendermint \
    -v $PWD/DB1:/DB \
    -p 45001:55000 \
    -p 46001:56000 \
    -p 47001:57000 \
    --name ofp-dpki-blockchain-1 \
    ofp/dpki-blockchain:latest

# curl
curl http://localhost:55001/net_info
curl http://localhost:55001/validators
curl http://localhost:55001/status

# Regulator: init_regulator
curl --location 'http://localhost:8999/api/dpki/init_regulator' \
--header 'Traceparent: 00-4b58915b5e4aeef1bd060ebf584c4560-3dc1dd0c9d1951cd-01' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer QpDMF0jHCZUWjUOQDC6gWdhF70NQ5Xel' \
--data '{
  "params": {
    "node_name": "GT"
  }
}'

# Regulator: add_node
curl --location 'http://localhost:8999/api/dpki/set_node' \
--header 'Traceparent: 00-4b58915b5e4aeef1bd060ebf584c4560-3dc1dd0c9d1951cd-01' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer QpDMF0jHCZUWjUOQDC6gWdhF70NQ5Xel' \
--data '{
  "params": {
    "node_id": "4595bca9-db94-4772-9adf-a7094e292f94",
    "node_name": "GT",
    "master_public_key": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArdoP8mOZqfDi1uokOjlw\n9ZBwvuAIQ3NdQZOms0W5ai+O8/QyQ0TblvfeLEmpYNKLCWfgaGRxbpRaNXSYvnbS\n0ldbU1Pz0aEQufP+AufVQVlDTVKn8ObSwgkEA/VJ9ZIaFe4uMSosV8fzmqrtXrXg\nW8hlLKCPizVJs4g6z2BmZxEzQS4I4c/00NOy3LdTwqdDfi5iZgOgMh+PE/iBS89x\nV3H0I9d5FbFtr0tgRSPE0K4dQIkZEAfRV7IccnZDNwWXoOqHjHkbU9UEOSQqI+wC\nB/WTnjN6Y5GaAFyADjFDZzFQTj5maN9p4Z6201aV2J6SI0DEd9c+5AtsKFgFoEUJ\nTQIDAQAB\n-----END PUBLIC KEY-----",
    "public_key_for_signature": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArdoP8mOZqfDi1uokOjlw\n9ZBwvuAIQ3NdQZOms0W5ai+O8/QyQ0TblvfeLEmpYNKLCWfgaGRxbpRaNXSYvnbS\n0ldbU1Pz0aEQufP+AufVQVlDTVKn8ObSwgkEA/VJ9ZIaFe4uMSosV8fzmqrtXrXg\nW8hlLKCPizVJs4g6z2BmZxEzQS4I4c/00NOy3LdTwqdDfi5iZgOgMh+PE/iBS89x\nV3H0I9d5FbFtr0tgRSPE0K4dQIkZEAfRV7IccnZDNwWXoOqHjHkbU9UEOSQqI+wC\nB/WTnjN6Y5GaAFyADjFDZzFQTj5maN9p4Z6201aV2J6SI0DEd9c+5AtsKFgFoEUJ\nTQIDAQAB\n-----END PUBLIC KEY-----",
    "public_key_for_encryption": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArdoP8mOZqfDi1uokOjlw\n9ZBwvuAIQ3NdQZOms0W5ai+O8/QyQ0TblvfeLEmpYNKLCWfgaGRxbpRaNXSYvnbS\n0ldbU1Pz0aEQufP+AufVQVlDTVKn8ObSwgkEA/VJ9ZIaFe4uMSosV8fzmqrtXrXg\nW8hlLKCPizVJs4g6z2BmZxEzQS4I4c/00NOy3LdTwqdDfi5iZgOgMh+PE/iBS89x\nV3H0I9d5FbFtr0tgRSPE0K4dQIkZEAfRV7IccnZDNwWXoOqHjHkbU9UEOSQqI+wC\nB/WTnjN6Y5GaAFyADjFDZzFQTj5maN9p4Z6201aV2J6SI0DEd9c+5AtsKFgFoEUJ\nTQIDAQAB\n-----END PUBLIC KEY-----",
    "role": "GT"
  }
}'

# Regulator: set_validator
curl --location 'http://localhost:8999/api/dpki/set_validator' \
--header 'Traceparent: 00-4b58915b5e4aeef1bd060ebf584c4560-3dc1dd0c9d1951cd-01' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer QpDMF0jHCZUWjUOQDC6gWdhF70NQ5Xel' \
--data '{
  "params": {
    "public_key": "oONnlPa72cpt66A/4Kfja0RruboZcqMk58IXe3B9W7g=",
    "power": 20
  }
}'

# DPKI: get_node_info
curl --location 'http://localhost:8071/api/get_node_info' \
--header 'Traceparent: 00-4b58915b5e4aeef1bd060ebf584c4560-3dc1dd0c9d1951cd-01' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer QpDMF0jHCZUWjUOQDC6gWdhF70NQ5Xel' \
--data '{
  "node_id": "c1b11ddb22fb5f35b190c3042ccb5324443866b2"
}'
